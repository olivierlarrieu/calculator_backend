import os
from django.test.runner import DiscoverRunner


class TestRunner(DiscoverRunner):
    """
    Allow testing in django application in apps directory
    """
    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        if not test_labels:
            test_labels = [d for d in os.listdir('apps') if os.path.isdir(os.path.join('apps', d))]
        else:
            test_labels = [i.replace('apps.', '') for i in test_labels]
        return super().run_tests(test_labels, extra_tests=None, **kwargs)
