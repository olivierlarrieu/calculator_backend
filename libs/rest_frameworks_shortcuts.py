from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.routers import DefaultRouter
from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet


def get_model_serializer_for(model_cls):
    class NewSerializer(ModelSerializer):
        class Meta:
            model = model_cls
            fields = "__all__"

    return NewSerializer


def get_model_viewset_for(model_cls, docstring):

    default_docstring = "auto generated ModelViewset for model {}.".format(model_cls.__name__)
    serializer = get_model_serializer_for(model_cls)

    class NewModelViewset(ModelViewSet):
        __doc__ = docstring or default_docstring
        queryset = model_cls.objects.all()
        serializer_class = serializer
        filter_backends = [
            DjangoFilterBackend,
            OrderingFilter,
            SearchFilter,
        ]
        filter_fields = [f.name for f in model_cls._meta.fields]
        search_fields = [f.name for f in model_cls._meta.fields]
        ordering_fields = [f.name for f in model_cls._meta.fields]

    return NewModelViewset


class DefaultRouter(DefaultRouter):
    def register_model(self, model, name=None, base_name=None, docstring=None):
        if name is None:
            raise Exception("name is missing")
        self.register(
            name,
            get_model_viewset_for(model, docstring=docstring),
            base_name=base_name or name
        )
