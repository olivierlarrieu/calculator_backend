import os
import sys
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.append(os.path.join(BASE_DIR, 'apps'))


CALCULATOR_ENV = os.environ.get('CALCULATOR_ENV')

if CALCULATOR_ENV == 'dev':
    from .develop import *
else:
    from .production import *
