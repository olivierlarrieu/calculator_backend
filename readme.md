# Requirements

    python 3.5

# Installation

    pip install -r configs/requirements.txt
    python manage.py makemigrations
    python manage.py migrate

# configuration

    for developpement settings activated:
        set environment variable CALCULATOR_ENV
        export CALCULATOR_ENV=dev
    if not set production settings are the default

