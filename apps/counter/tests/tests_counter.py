from rest_framework import status
from rest_framework.test import APITestCase

from counter.constants import OPERATORS, OPERATORS_DICT
from counter.models import Counter


class CounterTests(APITestCase):

    URL = '/apis/counter/'

    def test_post(self):
        # operator et value sont required
        response = self.client.post(self.URL, data={}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {'operator': ['This field is required.'], 'value': ['This field is required.']}
        )

        # tests des operateurs autorisés
        data = {'operator': '+', 'value': 10}
        response = self.client.post(self.URL, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {"result": 10.0})

        data = {'operator': '/', 'value': 5}
        response = self.client.post(self.URL, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {"result": 2.0})

        data = {'operator': '-', 'value': 10}
        response = self.client.post(self.URL, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {"result": -8.0})

        data = {'operator': '*', 'value': 2}
        response = self.client.post(self.URL, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {"result": -16.0})

        # test operateur non autorisé
        data = {'operator': '**', 'value': 2}
        response = self.client.post(self.URL, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'operator': ['"**" is not a valid choice.']})

        # test value non autorisé
        data = {'operator': '*', 'value': "un string ne doit pas etre autorisé"}
        response = self.client.post(self.URL, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'value': ['A valid number is required.']})
