from rest_framework import status
from rest_framework.test import APITestCase

from counter.constants import OPERATORS
from counter.models import Counter


class HistoryTests(APITestCase):

    URL = '/apis/history/'

    def setUp(self):

        for i in range(10):
            Counter.objects.create(
                operator=OPERATORS[0][1],
                value=i
            )
    
    def test_list(self):
        response = self.client.get(self.URL)
        response_data = response.json()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_data), 10)
        last_data = {
            'operator': '+',
            'result': 45.0,
            'value': 9.0
        }
        self.assertEqual(response_data[-1], last_data)

    def test_reset(self):
        URL = self.URL + '?action=reset'
        response = self.client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Counter.objects.count(), 0)
