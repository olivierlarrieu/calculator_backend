from rest_framework import serializers, status, viewsets
from rest_framework.response import Response

from counter.constants import OPERATORS_DICT, OPERATORS
from counter.models import Counter, SimpleProduct

OPERATOR_CHOICES = [v for k, v in OPERATORS]

class CounterSerializer(serializers.ModelSerializer):

    operator = serializers.ChoiceField(choices=OPERATOR_CHOICES)
    value = serializers.FloatField()

    def validate_operator(self, operator):
        if operator not in OPERATORS_DICT:
            raise serializers.ValidationError('< {} > is not allowed'.format(operator))
        return operator

    class Meta:
        model = Counter
        fields = ['operator', 'value', ]


class CounterViewSet(viewsets.ViewSet):

    serializer_class = CounterSerializer

    def list(self, request, **kwargs):
        counter_value = 0
        last_result = Counter.objects.last()
        if last_result is not None:
            counter_value = last_result.result
        return Response({"result": counter_value}, status=status.HTTP_200_OK)

    def create(self, request, **kwargs):
        # data validation
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        counter = serializer.save()

        # finally return response with status 200 ok
        return Response({"result": counter.result}, status=status.HTTP_200_OK)


class SimpleProductSerializer(serializers.ModelSerializer):

    id = serializers.UUIDField(required=False)
    class Meta:
        model = SimpleProduct
        fields = ['id', 'name', ]


class SimpleProductView(viewsets.ModelViewSet):
    queryset = SimpleProduct.objects.all()
    serializer_class = SimpleProductSerializer
