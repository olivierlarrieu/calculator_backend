from rest_framework import mixins, serializers, viewsets
from counter.models import Counter


class HistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Counter
        fields = ['operator', 'value', 'result', ]


class HistoryViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):

    queryset = Counter.history.all().order_by('history_date')
    serializer_class = HistorySerializer

    def list(self, request, **kwargs):
        action = request.query_params.get('action')
        if action is not None and action in ['reset', ]:
            Counter.clear_history()
        return super().list(request, **kwargs)
