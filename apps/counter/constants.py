from operator import add, sub, truediv, mul


OPERATORS = (
    (1, '+'),
    (2, '-'),
    (3, '/'),
    (4, '*'),
)

OPERATORS_DICT = {
    "+": add,
    "-": sub,
    "/": truediv,
    "*": mul
}
