import uuid
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from simple_history.models import HistoricalRecords

from counter.constants import OPERATORS, OPERATORS_DICT
from counter.settings import DEFAULT_COUNTER_VALUE


class Category(models.Model):
    name = models.CharField(max_length=50)


class SimpleProduct(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=20)
    category = models.ForeignKey(Category, null=True, blank=True ,on_delete=models.PROTECT, related_name="products")


class Counter(models.Model):

    operator = models.CharField(
        choices=OPERATORS,
        max_length=1,
        blank=False,
        null=False,
        help_text=_("operation operator")
    )
    value = models.FloatField(blank=False, null=False, help_text=_("operation value"))
    result = models.FloatField(blank=False, null=False, help_text=_("operation result"))
    history = HistoricalRecords()

    def __str__(self):
        return "{}".format(self.result)

    def save(self, *args, **kwargs):
        last_counter_result = DEFAULT_COUNTER_VALUE
        last_counter = Counter.objects.last()
        if last_counter is not None:
            last_counter_result = last_counter.result
        try:
            self.result = OPERATORS_DICT[self.operator](last_counter_result, self.value)
        except:
            message = "< {} {} {} > is not a valid operation".format(last_counter_result, self.operator, self.value)
            raise Exception(message)
        super().save(*args, **kwargs)

    @classmethod
    def clear_history(cls):
        cls.objects.all().delete()
        cls.history.all().delete()
