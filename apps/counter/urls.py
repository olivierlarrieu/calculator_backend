from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.counter.views import CounterViewSet, HistoryViewSet, SimpleProductView
from rest_frameworks_shortcuts import DefaultRouter
from counter.models import Category


router = DefaultRouter()

router.register(
    'counter',
    CounterViewSet,
    base_name='counter'
)

router.register(
    'history',
    HistoryViewSet,
    base_name='history'
)

router.register(
    'products',
    SimpleProductView,
    base_name='products'
)

router.register_model(Category, name='categories', )


urlpatterns = [
    path('', include(router.urls)),
]
